wget https://download.java.net/openjdk/jdk8u41/ri/openjdk-8u41-b04-linux-x64-14_jan_2020.tar.gz
tar -xvf openjdk-8u41-b04-linux-x64-14_jan_2020.tar.gz
mv jdk-13.0.1 /opt/

JAVA_HOME='/opt/jdk-13.0.1'
PATH="$JAVA_HOME/bin:$PATH"
export PATH

java -version